package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tsc.abzalov.tm.api.repository.ISessionRepository;
import tsc.abzalov.tm.model.Session;

import static org.junit.jupiter.api.Assertions.*;

class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final Session session = new Session();

    @BeforeEach
    void setUp() {
        sessionRepository.addSession(session);
    }

    @AfterEach
    void tearDown() {
        sessionRepository.removeSession(session);
    }

    @Test
    @DisplayName("Open Session Test")
    void addSession() {
        @Nullable val openedSession = sessionRepository.findSession(session);
        assertAll(
                () -> assertNotNull(openedSession),
                () -> assertEquals(session, openedSession)
        );
    }

    @Test
    @DisplayName("Close Session Test")
    void removeSession() {
        sessionRepository.removeSession(session);
        @Nullable val removedSession = sessionRepository.findSession(session);
        assertNull(removedSession);
    }

    @Test
    @DisplayName("Find Session Test")
    void findSession() {
        @Nullable val foundedSession = sessionRepository.findSession(session);
        @NotNull val newSession = new Session();
        @Nullable val anotherSession = sessionRepository.findSession(newSession);

        assertAll(
                () -> assertEquals(session, foundedSession),
                () -> assertNull(anotherSession)
        );
    }
}
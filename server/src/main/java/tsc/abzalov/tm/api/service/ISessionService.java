package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Session;

public interface ISessionService {

    @NotNull
    Session openSession(@Nullable String login, @Nullable String password);

    void closeSession(@Nullable Session session);

    void validate(@Nullable Session session);

    void validateAdminPermissions(@Nullable Session session, @NotNull IUserService userService);

    @NotNull
    Session findSession(@Nullable Session session);

}

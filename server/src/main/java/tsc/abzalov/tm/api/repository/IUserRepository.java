package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IRepository;
import tsc.abzalov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User editPassword(@NotNull String id, @NotNull String hashedPassword);

    @Nullable
    User editUserInfo(@NotNull String id, @NotNull String firstName, @Nullable String lastName);

    void deleteByLogin(@NotNull String login);

    @Nullable
    User lockUnlockById(@NotNull String id);

    @Nullable
    User lockUnlockByLogin(@NotNull String login);

}

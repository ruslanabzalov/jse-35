package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.model.Project;

public interface IProjectRepository extends IBusinessEntityRepository<Project> {
}

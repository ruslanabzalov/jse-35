package tsc.abzalov.tm.api.service;

public interface IFileBackupService {

    void autoLoadBackup();

    void autoSaveBackup();

    void base64LoadBackup();

    void base64SaveBackup();

    void binaryLoadBackup();

    void binarySaveBackup();

    void fasterXmlJsonLoadBackup();

    void fasterXmlJsonSaveBackup();

    void fasterXmlLoadBackup();

    void fasterXmlSaveBackup();

    void fasterXmlYamlLoadBackup();

    void fasterXmlYamlSaveBackup();

    void jaxbJsonLoadBackup();

    void jaxbJsonSaveBackup();

    void jaxbXmlLoadBackup();

    void jaxbXmlSaveBackup();

}

package tsc.abzalov.tm.endpoint;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;
import tsc.abzalov.tm.api.endpoint.IUserEndpoint;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
    }

    public UserEndpoint(@NotNull final IEndpointLocator endpointLocator) {
        super(endpointLocator);
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public User findUserById(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val userService = getEndpointLocator().getUserService();
        return userService.findById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public User editPasswordById(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "password") @Nullable final String newPassword) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val userService = getEndpointLocator().getUserService();
        return userService.editPasswordById(session.getUserId(), newPassword);
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public User editUserInfoById(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "firstName") @Nullable final String firstName,
                                 @WebParam(name = "lastName") @Nullable final String lastName) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val userService = getEndpointLocator().getUserService();
        return userService.editUserInfoById(session.getUserId(), firstName, lastName);
    }

}

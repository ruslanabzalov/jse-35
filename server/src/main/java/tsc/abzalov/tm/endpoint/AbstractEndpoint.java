package tsc.abzalov.tm.endpoint;

import lombok.AccessLevel;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;

@Getter(value = AccessLevel.PROTECTED)
public abstract class AbstractEndpoint {

    @Nullable
    private IEndpointLocator endpointLocator;

    public AbstractEndpoint() {
    }

    public AbstractEndpoint(@NotNull final IEndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

}

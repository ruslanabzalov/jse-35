package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Optional;

import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_REFERENCE;

@Data
@EqualsAndHashCode(callSuper = true)
public final class Task extends AbstractBusinessEntity implements Serializable, Cloneable {

    @Nullable
    private String projectId;

    @Nullable
    @Override
    public Task clone() {
        try {
            return (Task) super.clone();
        }
        catch (@NotNull final CloneNotSupportedException exception) {
            return null;
        }
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull val correctProjectId = Optional.ofNullable(projectId).orElse(DEFAULT_REFERENCE);
        @NotNull val superStringInterpretation = super.toString();
        return superStringInterpretation.replace("]", "; Project ID: " + correctProjectId + "]");
    }

}

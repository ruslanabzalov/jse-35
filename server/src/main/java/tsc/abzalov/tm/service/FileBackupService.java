package tsc.abzalov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import lombok.val;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IFileBackupService;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.domain.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static tsc.abzalov.tm.util.SystemUtil.BASE64_DECODER;
import static tsc.abzalov.tm.util.SystemUtil.BASE64_ENCODER;

public final class FileBackupService implements IFileBackupService {

    @NotNull
    public static final String BINARY_FILENAME = "data.bin";

    @NotNull
    public static final String BASE64_FILENAME = "data.base64";

    @NotNull
    public static final String FASTERXML_XML_FILENAME = "data_fasterxml.xml";

    @NotNull
    public static final String FASTERXML_JSON_FILENAME = "data_fasterxml.json";

    @NotNull
    public static final String FASTERXML_YAML_FILENAME = "data_fasterxml.yml";

    @NotNull
    public static final String JAXB_XML_FILENAME = "data_jaxb.xml";

    @NotNull
    public static final String JAXB_JSON_FILENAME = "data_jaxb.json";

    @NotNull
    public static final String BACKUP_FILENAME = "backup.json";

    @NotNull
    public static final String JAXB_CONTEXT_FACTORY_PROPERTY_NAME = "javax.xml.bind.context.factory";

    @NotNull
    public static final String JAXB_CONTEXT_FACTORY_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String JAXB_MEDIA_TYPE = "application/json";

    @NotNull
    private final IServiceLocator serviceLocator;

    public FileBackupService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public void autoLoadBackup() {
        @NotNull val objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        @NotNull val file = new File(BACKUP_FILENAME);

        if (!file.exists()) return;

        @NotNull val domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void autoSaveBackup() {
        @NotNull val domain = getDomain();

        @NotNull val file = new File(BACKUP_FILENAME);
        @NotNull val filePath = file.toPath();
        Files.deleteIfExists(filePath);
        Files.createFile(filePath);

        @NotNull val objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        @NotNull val objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        objectWriter.writeValue(file, domain);
    }

    @Override
    @SneakyThrows
    public void base64LoadBackup() {
        @NotNull val base64Data = new String(Files.readAllBytes(Paths.get(BASE64_FILENAME)));
        val decodedBase64Data = BASE64_DECODER.decode(base64Data);

        @NotNull val byteArrayInputStream = new ByteArrayInputStream(decodedBase64Data);
        @NotNull val objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull val domain = (Domain) objectInputStream.readObject();
        setDomain(domain);

        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    @SneakyThrows
    public void base64SaveBackup() {
        @NotNull val domain = getDomain();

        @NotNull val file = new File(BASE64_FILENAME);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull val byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull val objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        val domainBytes = byteArrayOutputStream.toByteArray();
        val base64Domain = BASE64_ENCODER.encode(domainBytes);

        @NotNull val fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64Domain);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void binaryLoadBackup() {
        @NotNull val fileInputStream = new FileInputStream(BINARY_FILENAME);
        @NotNull val objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull val domain = (Domain) objectInputStream.readObject();
        setDomain(domain);

        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    @SneakyThrows
    public void binarySaveBackup() {
        @NotNull val domain = getDomain();

        @NotNull val file = new File(BINARY_FILENAME);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull val fileOutputStream = new FileOutputStream(file);
        @NotNull val objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);

        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void fasterXmlJsonLoadBackup() {
        @NotNull val objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        @NotNull val file = new File(FASTERXML_JSON_FILENAME);

        if (!file.exists()) {
            @NotNull val fileName = file.getName();
            throw new FileNotFoundException("File " + fileName + " is not exist!");
        }

        @NotNull val domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void fasterXmlJsonSaveBackup() {
        @NotNull val domain = getDomain();

        @NotNull val file = new File(FASTERXML_JSON_FILENAME);
        @NotNull val filePath = file.toPath();
        Files.deleteIfExists(filePath);
        Files.createFile(filePath);

        @NotNull val objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        @NotNull val objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        objectWriter.writeValue(file, domain);
    }

    @Override
    @SneakyThrows
    public void fasterXmlLoadBackup() {
        @NotNull val xmlMapper = new XmlMapper().registerModule(new JavaTimeModule());
        @NotNull val file = new File(FASTERXML_XML_FILENAME);

        if (!file.exists()) {
            @NotNull val fileName = file.getName();
            throw new FileNotFoundException("File " + fileName + " is not exist!");
        }

        @NotNull val domain = xmlMapper.readValue(file, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void fasterXmlSaveBackup() {
        @NotNull val domain = getDomain();

        @NotNull val file = new File(FASTERXML_XML_FILENAME);
        @NotNull val filePath = file.toPath();
        Files.deleteIfExists(filePath);
        Files.createFile(filePath);

        @NotNull val xmlMapper = new XmlMapper().registerModule(new JavaTimeModule());
        @NotNull val objectWriter = xmlMapper.writerWithDefaultPrettyPrinter();
        objectWriter.writeValue(file, domain);
    }

    @Override
    @SneakyThrows
    public void fasterXmlYamlLoadBackup() {
        @NotNull val objectMapper = new ObjectMapper(new YAMLFactory())
                .registerModule(new JavaTimeModule());
        @NotNull val file = new File(FASTERXML_YAML_FILENAME);

        if (!file.exists()) {
            @NotNull val fileName = file.getName();
            throw new FileNotFoundException("File " + fileName + " is not exist!");
        }

        @NotNull val domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void fasterXmlYamlSaveBackup() {
        @NotNull val domain = getDomain();

        @NotNull val file = new File(FASTERXML_YAML_FILENAME);
        @NotNull val filePath = file.toPath();
        Files.deleteIfExists(filePath);
        Files.createFile(filePath);

        @NotNull val objectMapper = new ObjectMapper(new YAMLFactory())
                .registerModule(new JavaTimeModule());
        @NotNull val objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        objectWriter.writeValue(file, domain);
    }

    @Override
    @SneakyThrows
    public void jaxbJsonLoadBackup() {
        System.setProperty(JAXB_CONTEXT_FACTORY_PROPERTY_NAME, JAXB_CONTEXT_FACTORY_PROPERTY_VALUE);

        @NotNull val file = new File(JAXB_JSON_FILENAME);
        if (!file.exists()) {
            @NotNull val fileName = file.getName();
            throw new FileNotFoundException("File " + fileName + " is not exist!");
        }

        @NotNull val context = JAXBContext.newInstance(Domain.class);
        @NotNull val unmarshaller = context.createUnmarshaller();

        unmarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, JAXB_MEDIA_TYPE);
        unmarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        @Nullable val domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void jaxbJsonSaveBackup() {
        System.setProperty(JAXB_CONTEXT_FACTORY_PROPERTY_NAME, JAXB_CONTEXT_FACTORY_PROPERTY_VALUE);

        @NotNull val context = JAXBContext.newInstance(Domain.class);
        @NotNull val marshaller = context.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, JAXB_MEDIA_TYPE);
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        @NotNull val file = new File(JAXB_JSON_FILENAME);
        @NotNull val filePath = file.toPath();
        Files.deleteIfExists(filePath);
        Files.createFile(filePath);

        @NotNull val domain = getDomain();
        marshaller.marshal(domain, file);
    }

    @Override
    @SneakyThrows
    public void jaxbXmlLoadBackup() {
        @NotNull val file = new File(JAXB_XML_FILENAME);
        if (!file.exists()) {
            @NotNull val fileName = file.getName();
            throw new FileNotFoundException("File " + fileName + " is not exist!");
        }

        @NotNull val context = JAXBContext.newInstance(Domain.class);
        @NotNull val unmarshaller = context.createUnmarshaller();

        @Nullable val domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void jaxbXmlSaveBackup() {
        @NotNull val context = JAXBContext.newInstance(Domain.class);
        @NotNull val marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        @NotNull val file = new File(JAXB_XML_FILENAME);
        @NotNull val filePath = file.toPath();
        Files.deleteIfExists(filePath);
        Files.createFile(filePath);

        @NotNull val domain = getDomain();
        marshaller.marshal(domain, file);
    }

    @NotNull
    private Domain getDomain() {
        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val taskService = serviceLocator.getTaskService();
        @NotNull val userService = serviceLocator.getUserService();
        @NotNull val projects = projectService.findAll();
        @NotNull val tasks = taskService.findAll();
        @NotNull val users = userService.findAll();

        @NotNull val domain = new Domain();
        domain.setProjects(projects);
        domain.setTasks(tasks);
        domain.setUsers(users);
        return domain;
    }

    private void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;

        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val taskService = serviceLocator.getTaskService();
        @NotNull val userService = serviceLocator.getUserService();

        projectService.clear();
        projectService.addAll(domain.getProjects());

        taskService.clear();
        taskService.addAll(domain.getTasks());

        userService.clear();
        userService.addAll(domain.getUsers());
    }

}

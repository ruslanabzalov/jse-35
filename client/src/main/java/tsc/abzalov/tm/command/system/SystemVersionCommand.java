package tsc.abzalov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;


public final class SystemVersionCommand extends AbstractCommand {

    public SystemVersionCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "version";
    }

    @Override
    @NotNull
    public String getCommandArgument() {
        return "-v";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Shows application version.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val propertyService = serviceLocator.getPropertyService();
        @NotNull val appVersion = propertyService.getAppVersion();
        @NotNull val manifestBuild = Manifests.read("Build");

        System.out.println("Version: " + appVersion);
        System.out.println("Manifest Build: " + manifestBuild + "\n");
    }

}

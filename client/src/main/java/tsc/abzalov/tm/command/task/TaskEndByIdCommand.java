package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;


public final class TaskEndByIdCommand extends AbstractCommand {

    public TaskEndByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "end-task-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "End task by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("END TASK BY ID");
        @NotNull val taskEndpoint = getServiceLocator().getTaskEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        val areEntitiesExist = taskEndpoint.tasksSize(session) != 0;

        if (areEntitiesExist) {
            @Nullable val task = taskEndpoint.endTaskById(session, inputId());
            val isTaskExist = Optional.ofNullable(task).isPresent();
            if (isTaskExist) {
                System.out.println("Task was successfully ended.\n");
                return;
            }

            System.out.println("Task was not ended! Please, check that task exists or it has correct status.\n");
            return;
        }

        System.out.println("Tasks are not exist.\n");
    }

}

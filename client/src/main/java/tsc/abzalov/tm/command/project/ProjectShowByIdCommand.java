package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;


public final class ProjectShowByIdCommand extends AbstractCommand {

    public ProjectShowByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-project-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show project by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("FIND PROJECT BY ID");
        @NotNull val projectEndpoint = getServiceLocator().getProjectEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        val areProjectsExist = projectEndpoint.projectsSize(session) != 0;
        if (areProjectsExist) {
            @NotNull val projectId = inputId();
            System.out.println();

            @Nullable val searchedProject = projectEndpoint.findProjectById(session, projectId);
            val isProjectExist = Optional.ofNullable(searchedProject).isPresent();
            if (isProjectExist) {
                val projectIndex = projectEndpoint.projectIndex(session, searchedProject) + 1;
                System.out.println(projectIndex + ". " + searchedProject + "\n");
                return;
            }

            System.out.println("Searched project was not found.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}

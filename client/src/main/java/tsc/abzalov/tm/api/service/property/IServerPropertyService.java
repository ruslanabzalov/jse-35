package tsc.abzalov.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IServerPropertyService {

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

}

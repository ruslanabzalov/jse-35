package tsc.abzalov.tm.endpoint;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AdminEndpointTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void sizeUsers() {
    }

    @Test
    void isUserExist() {
    }

    @Test
    void findUserByLogin() {
    }

    @Test
    void lockUnlockUserByLogin() {
    }

    @Test
    void removeUserById() {
    }

    @Test
    void createUserWithEntity() {
    }

    @Test
    void lockUnlockUserById() {
    }

    @Test
    void addAllUsers() {
    }

    @Test
    void editUserInfoById() {
    }

    @Test
    void deleteUserByLogin() {
    }

    @Test
    void createUser() {
    }

    @Test
    void isEmptyUserList() {
    }

    @Test
    void findAllUsers() {
    }

    @Test
    void findUserById() {
    }

    @Test
    void findUsersById() {
    }

    @Test
    void editPasswordById() {
    }

    @Test
    void createUserWithCustomRole() {
    }

    @Test
    void clearAllUsers() {
    }
}
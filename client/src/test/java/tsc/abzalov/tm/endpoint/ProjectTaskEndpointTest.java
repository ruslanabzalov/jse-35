package tsc.abzalov.tm.endpoint;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectTaskEndpointTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void deleteProjectById() {
    }

    @Test
    void findProjectTasksById() {
    }

    @Test
    void addTaskToProjectById() {
    }

    @Test
    void findTaskProjectById() {
    }

    @Test
    void deleteProjectTaskById() {
    }

    @Test
    void findProjectTaskById() {
    }

    @Test
    void hasData() {
    }

    @Test
    void deleteProjectTasksById() {
    }

    @Test
    void indexOfProjectTask() {
    }
}
package tsc.abzalov.tm.endpoint;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectEndpointTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createProject() {
    }

    @Test
    void removeProjectByName() {
    }

    @Test
    void addAllProjects() {
    }

    @Test
    void areProjectsEmpty() {
    }

    @Test
    void removeProjectByIndex() {
    }

    @Test
    void sortProjectsByName() {
    }

    @Test
    void clearProjects() {
    }

    @Test
    void editProjectByIndex() {
    }

    @Test
    void projectsSize() {
    }

    @Test
    void findAllProjects() {
    }

    @Test
    void sortProjectsByStatus() {
    }

    @Test
    void clearAllProjects() {
    }

    @Test
    void editProjectById() {
    }

    @Test
    void findProjectByIndex() {
    }

    @Test
    void findProjectsById() {
    }

    @Test
    void findProjectById() {
    }

    @Test
    void editProjectByName() {
    }

    @Test
    void sortProjectsByEndDate() {
    }

    @Test
    void findProjectByName() {
    }

    @Test
    void sortProjectsByStartDate() {
    }

    @Test
    void removeProjectById() {
    }

    @Test
    void startProjectById() {
    }

    @Test
    void findAllProjectsById() {
    }

    @Test
    void projectIndex() {
    }

    @Test
    void endProjectById() {
    }

    @Test
    void sizeProjects() {
    }

    @Test
    void isEmptyProjectList() {
    }
}
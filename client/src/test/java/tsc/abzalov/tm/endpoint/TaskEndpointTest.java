package tsc.abzalov.tm.endpoint;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskEndpointTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void sortTasksByStartDate() {
    }

    @Test
    void editTaskById() {
    }

    @Test
    void taskIndex() {
    }

    @Test
    void findTasksById() {
    }

    @Test
    void findAllTasks() {
    }

    @Test
    void isEmptyTaskList() {
    }

    @Test
    void removeTaskByIndex() {
    }

    @Test
    void findTaskByName() {
    }

    @Test
    void editTaskByName() {
    }

    @Test
    void clearAllTasks() {
    }

    @Test
    void endTaskById() {
    }

    @Test
    void sortTasksByStatus() {
    }

    @Test
    void addAllTasks() {
    }

    @Test
    void sortTasksByName() {
    }

    @Test
    void findTaskById() {
    }

    @Test
    void clearTasks() {
    }

    @Test
    void startTaskById() {
    }

    @Test
    void findTaskByIndex() {
    }

    @Test
    void removeTaskById() {
    }

    @Test
    void tasksSize() {
    }

    @Test
    void createTask() {
    }

    @Test
    void areTasksEmpty() {
    }

    @Test
    void removeTaskByName() {
    }

    @Test
    void sortTasksByEndDate() {
    }

    @Test
    void editTaskByIndex() {
    }

    @Test
    void sizeTasks() {
    }
}